module.exports = (sequelize, Datatypes) => {
    return sequelize .define('Product',{
        id: {
            type : Datatypes.INTEGER,
            primaryKey : true,
            autoIncrement : true,
        },
        productName: {
            type : Datatypes.STRING,
            allowNull : false,
        },
        productImage : {
            type : Datatypes.STRING,
            allowNull : false,
        },
        productCurrentPrice : {
            type : Datatypes.INTEGER,
            allowNull : false,  
        },
        productPastPrice : {
            type : Datatypes.INTEGER,
            allowNull : false,
        },     
        productNote: {
            type : Datatypes.INTEGER,
            allowNull : false
        },
        productViews : {
            type : Datatypes.INTEGER,
            allowNull: false
        },
        productDescription: {
            type : Datatypes.STRING,
            allowNull : false,
        },
        productCategory : {
            type : Datatypes.STRING,
            allowNull: false
        },
        productColorTable : {
            type : Datatypes.STRING,
            allowNull : false,
        },
        productSizeTable : {
            type : Datatypes.STRING,
            allowNull : false
        }
    });
}