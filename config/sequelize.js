const {Sequelize, DataTypes} = require('sequelize');
const UserModel = require('../models/userModel')
const ProductModel = require('../models/productModel');
const OrderModel = require('../models/commandeModel');

const sequelize = new Sequelize('ecommerce', 'root','',{
    host: 'localhost',
    dialect : 'mariadb',
    logging : false,
});

const User = UserModel(sequelize, DataTypes);
const Product = ProductModel(sequelize, DataTypes);
const Order = OrderModel(sequelize, DataTypes);

const initDb()=>{
    
}